import argparse
import math

import cv2
import numpy as np
import torch
from modules.keypoints import extract_keypoints, group_keypoints
from modules.pose import Pose, track_poses
from modules.yolomodel import YOLOv8

import onnxruntime as ort

class ImageReader(object):
    def __init__(self, file_names):
        self.file_names = file_names
        self.max_idx = len(file_names)

    def __iter__(self):
        self.idx = 0
        return self

    def __next__(self):
        if self.idx == self.max_idx:
            raise StopIteration
        img = cv2.imread(self.file_names[self.idx], cv2.IMREAD_COLOR)
        if img.size == 0:
            raise IOError('Image {} cannot be read'.format(self.file_names[self.idx]))
        self.idx = self.idx + 1
        return img


class VideoReader(object):
    def __init__(self, file_name):
        self.file_name = file_name
        try:  # OpenCV needs int to read from webcam
            self.file_name = int(file_name)
        except ValueError:
            pass

    def __iter__(self):
        self.cap = cv2.VideoCapture(self.file_name)
        if not self.cap.isOpened():
            raise IOError('Video {} cannot be opened'.format(self.file_name))
        return self

    def __next__(self):
        was_read, img = self.cap.read()
        if not was_read:
            raise StopIteration
        return img
def pose_preprocess(img, stride, base_height=368, base_width=640, pad_value=(0, 0, 0),
                  img_mean=np.array([128, 128, 128], np.float32), img_scale=np.float32(1/256)):
    height, width, _ = img.shape

    height_scale = base_height / height
    width_scale = base_width / width
    scale = min(height_scale, width_scale)
    scaled_img = cv2.resize(img, (0, 0), fx=scale, fy=scale, interpolation=cv2.INTER_LINEAR)
    scaled_img = normalize(scaled_img, img_mean, img_scale)
    min_dims = [base_height, base_width]
    padded_img, pad = pad_width(scaled_img, stride, pad_value, min_dims)
    tensor_img = torch.from_numpy(padded_img).permute(2, 0, 1).unsqueeze(0).float()
    img1 = np.array(tensor_img).astype(np.float32)

    return img1, scale, pad

def post_process():
    pass
def pad_width(img, stride, pad_value, min_dims):
    h, w, _ = img.shape
    h = min(min_dims[0], h)
    min_dims[0] = math.ceil(min_dims[0] / float(stride)) * stride
    min_dims[1] = max(min_dims[1], w)
    min_dims[1] = math.ceil(min_dims[1] / float(stride)) * stride
    pad = []
    pad.append(int(math.floor((min_dims[0] - h) / 2.0)))
    pad.append(int(math.floor((min_dims[1] - w) / 2.0)))
    pad.append(int(min_dims[0] - h - pad[0]))
    pad.append(int(min_dims[1] - w - pad[1]))
    padded_img = cv2.copyMakeBorder(img, pad[0], pad[2], pad[1], pad[3],
                                    cv2.BORDER_CONSTANT, value=pad_value)
    return padded_img, pad
def normalize(img, img_mean, img_scale):
    img = np.array(img, dtype=np.float32)
    img = (img - img_mean) * img_scale
    return img

def calculate_distance(port1, port2):
    distance = math.sqrt((port2[0] - port1[0])**2 + (port2[1] - port1[1])**2)
    return distance

def run_demo_pose(ort_session_pose, ort_session_dect, image_provider, cpu, track, smooth,  base_height=3000, base_width=4000,
                  cut_list=[1100, 800, 3000, 2700], pad_value=(0, 0, 0), img_mean=np.array([128, 128, 128], np.float32),
                  img_scale=np.float32(1/256)):
    stride = 8
    upsample_ratio = 4
    is_capture = False
    num = 0
    FPS=25
    battery_site = [0, 0]
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    out = cv2.VideoWriter('output_video.mp4', fourcc, 25, (base_width, base_height))
    for img in image_provider:
        if(is_capture and (not FPS)):
            out.write(img)
            FPS -= 1
        elif(FPS > 0):
            FPS -= 1
        elif(FPS == 0):
            FPS = 25
            x1, y1, x2, y2 = cut_list
            orig_img = img.copy()
            orig_img = orig_img[y1: y2, x1: x2]
            #output_image = detection.main(orig_img)
            input_image, indices, class_ids, scores, boxes = detection.main(orig_img)
            class_num_2 = []
            for i in indices:
                if int(class_ids[i]) == 2:
                    class_num_2.append(i)
            while class_num_2:
                temp = boxes[class_num_2[0]][:2]
                if num == 0:
                    battery_site = temp
                    num += 1
                    break
                distance = calculate_distance(battery_site, boxes[:2])
                if distance <= 100:
                    num += 1
                    if num > 3:
                        is_capture = True
                        out.write(img)
                    break
                elif distance > 100:
                    if len(class_num_2) > 1:
                        class_num_2.pop(0)
                    else:
                        num =0
                        out.release()
                        out = cv2.VideoWriter('output_video.mp4', fourcc, 25, (base_width, base_height))




            '''
            cv2.namedWindow("Output", cv2.WINDOW_NORMAL)
            cv2.imshow("Output", input_image)
            cv2.imwrite("111.jpg", input_image)
            # Wait for a key press to exit
            cv2.waitKey(0)
            '''

        '''
        height, width, _ = img.shape

        height_scale = base_height / height
        width_scale = base_width / width
        scale = min(height_scale, width_scale)
        scaled_img = cv2.resize(img, (0, 0), fx=scale, fy=scale, interpolation=cv2.INTER_LINEAR)
        scaled_img = normalize(scaled_img, img_mean, img_scale)
        min_dims = [base_height, base_width]
        padded_img, pad = pad_width(scaled_img, stride, pad_value, min_dims)
        tensor_img = torch.from_numpy(padded_img).permute(2, 0, 1).unsqueeze(0).float()
        img1 = np.array(tensor_img).astype(np.float32)
        '''
        img1 = pose_preprocess(img, stride)
        out, scale, pad = ort_session_pose.run(None, input_feed={ort_session_pose.get_inputs()[0].name: img1})

        print(len(out), out[0], "++++++++++++++++++++++++")
        npout_heatmaps = np.transpose(np.squeeze(np.array(out[2]), axis=0),(1, 2, 0))
        npout_heatmaps = cv2.resize(npout_heatmaps, (0, 0), fx=stride, fy=stride, interpolation=cv2.INTER_CUBIC)
        npout_pafs = np.transpose(np.squeeze(np.array(out[3]), axis=0), (1, 2, 0))
        npout_pafs = cv2.resize(npout_pafs, (0, 0), fx=stride, fy=stride, interpolation=cv2.INTER_CUBIC)
        print(npout_heatmaps.shape, npout_pafs.shape)

        np.save('infer_output_heatmaps.npy', npout_heatmaps)
        np.save('infe_output_pafs.npy', npout_pafs)

        num_keypoints = Pose.num_kpts
        total_keypoints_num = 0
        all_keypoints_by_type = []
        for kpt_idx in range(num_keypoints):  # 19th for bg
            total_keypoints_num += extract_keypoints(npout_heatmaps[:, :, kpt_idx], all_keypoints_by_type, total_keypoints_num)

        print(total_keypoints_num,"+++++++++++++++")

        pose_entries, all_keypoints = group_keypoints(all_keypoints_by_type, npout_pafs)
        for kpt_id in range(all_keypoints.shape[0]):
            all_keypoints[kpt_id, 0] = (all_keypoints[kpt_id, 0] * stride / upsample_ratio - pad[1]) / scale
            all_keypoints[kpt_id, 1] = (all_keypoints[kpt_id, 1] * stride / upsample_ratio - pad[0]) / scale
        current_poses = []
        for n in range(len(pose_entries)):
            if len(pose_entries[n]) == 0:
                continue
            pose_keypoints = np.ones((num_keypoints, 2), dtype=np.int32) * -1
            for kpt_id in range(num_keypoints):
                if pose_entries[n][kpt_id] != -1.0:  # keypoint was found
                    pose_keypoints[kpt_id, 0] = int(all_keypoints[int(pose_entries[n][kpt_id]), 0])
                    pose_keypoints[kpt_id, 1] = int(all_keypoints[int(pose_entries[n][kpt_id]), 1])
            pose = Pose(pose_keypoints, pose_entries[n][18])
            current_poses.append(pose)
        print(current_poses, "///////////////////")

        if track:
            track_poses(orig_img, current_poses, smooth=smooth)
            previous_poses = current_poses
        for pose in current_poses:
            pose.draw(img)
        cv2.imshow('11111', img)
        cv2.waitKey(0)
        cv2.imshow('22222', orig_img)
        cv2.waitKey(0)
        img = cv2.addWeighted(orig_img, 0.6, img, 0.4, 0)
        print(img.shape, "//////////////")
        #for pose in current_poses:
            #cv2.rectangle(img, (pose.bbox[0], pose.bbox[1]),
            #              (pose.bbox[0] + pose.bbox[2], pose.bbox[1] + pose.bbox[3]), (0, 255, 0))
            #if track:
            #    cv2.putText(img, 'id: {}'.format(pose.id), (pose.bbox[0], pose.bbox[1] - 16),
            #                cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 0, 255))
        cv2.imshow('Lightweight Human Pose Estimation Python Demo', img)
        key = cv2.waitKey(0)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='''Lightweight human pose estimation python demo.
                       This is just for quick results preview.
                       Please, consider c++ demo for the best performance.''')
    #parser.add_argument('--checkpoint-path', type=str, required=True, help='path to the checkpoint')
    #parser.add_argument('--height-size', type=int, default=256, help='network input layer height size')
    parser.add_argument('--region', type=str, default='1,2,3,4', help='the portion of the image that is being processed')
    parser.add_argument('--video', type=str, default='', help='path to video file or camera id')
    parser.add_argument('--images', nargs='+', default='', help='path to input image(s)')
    parser.add_argument('--cpu', action='store_true', help='run network inference on cpu')
    parser.add_argument('--track', type=int, default=1, help='track pose id in video')
    parser.add_argument('--smooth', type=int, default=1, help='smooth pose keypoints')
    parser.add_argument("--conf-thres", type=float, default=0.5, help="Confidence threshold")
    parser.add_argument("--iou-thres", type=float, default=0.5, help="NMS IoU threshold")
    args = parser.parse_args()

    if args.cpu:
        providers = ['CPUExecutionProvider']
    else:
        providers = ['CUDAExecutionProvider']


    ort_session_pose = ort.InferenceSession("human-pose-estimation.onnx", providers=providers)

    #input_name = ort_session_pose.get_inputs()[0].name
    ort_session_dect = ort.InferenceSession(r"D:\Ttmp\pythontest\openpose\ultralytics\yolov8s.onnx", providers=providers)
    detection = YOLOv8(r"D:\Ttmp\pythontest\openpose\ultralytics\yolov8s.onnx", args.conf_thres, args.iou_thres)



    if args.video != '':
        frame_provider = VideoReader(args.video)
    else:
        args.track = 0
        frame_provider = ImageReader(args.images)

    run_demo_pose(ort_session_pose, ort_session_dect, frame_provider,
                  args.region, args.region, args.cpu, args.track, args.smooth)